letras = ["A", "B", "C", "D", "E", "F"]
filasM = 5
columnasM = 5
matriz = []



def tableroBuscaMinas(filasMatriz,columnasMatriz):
    for fila in range(filasMatriz):# Aquí carga la matriz
        matriz.append([])
        for columna in range(columnasMatriz):
            matriz[fila].append(0)

    x = 0
    y = 0
    xAux = 0 # Se crea una variable auxiliar para para validar que ya el indice 0,0 este en blanco
    for j in range(filasMatriz):
        y = 0
        matriz[j][y] = j
        for k in range(columnasMatriz):
            if xAux == 0 and y == 0:# Se coloca un espcio en blanco en el indice 0,0 de la fila de resferencia para el usuario
                matriz[x][y] = " "
                xAux += 1
                y += 1
            elif xAux != 0 and y != 0 and j <1: # Se muestran letras guías eje x
                x = 0
                matriz[x][y] = letras[y - 1]
                y += 1

            if matriz[0][k] != matriz[j][k] and matriz[j][0] != matriz[j][k]: # Aquí se agregan "-" al tablero
                print(" ","-", end=" ")
            else:
                print(" ",matriz[j][k],end=" ") # Se muestran numero guías para el eje y
        print()

tableroBuscaMinas(5,5)
