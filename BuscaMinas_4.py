import random as rd

letras = ["A", "B", "C", "D", "E", "F"]
ubicacionesJugadas = []
matriz = []
puntos = 0 #Puntos por partida
puntosGeneral = 0 #Puntoa totales
nivel = 1
perdio = False

def crearTablero(filasMatriz, columnasMatriz):
    for fila in range(filasMatriz):  # Aquí carga la matriz
        matriz.append([])
        for columna in range(columnasMatriz):
            matriz[fila].append('.')

def tableroBuscaMinas(modoJuego):
    x = 0
    y = 0
    xAux = 0  # Se crea una variable auxiliar para para validar que ya el indice 0,0 este en blanco
    for j in range(len(matriz)):
        y = 0
        matriz[j][y] = j
        for k in range(len(matriz)):
            if xAux == 0 and y == 0:  # Se coloca un espcio en blanco en el indice 0,0 de la fila de resferencia para el usuario
                matriz[x][y] = " "
                xAux += 1
                y += 1
            elif xAux != 0 and y != 0 and j < 1:  # Se muestran letras guías eje x
                x = 0
                matriz[x][y] = letras[y - 1]
                y += 1
            if modoJuego == 2 and matriz[0][k] != matriz[j][k] and matriz[j][0] != matriz[j][k] and (matriz[j][k] == "." or matriz[j][k] == "*"):#Esconde la bomba para el modo de juego 2
                #if matriz[0][k] != matriz[j][k] and matriz[j][0] != matriz[j][k] and matriz[j][k] == "." and matriz[j][k] == '*':  # Aquí se agregan "-" al tablero
                print(" ", "-", end=" ")
                continue
            if modoJuego == 1 and matriz[0][k] != matriz[j][k] and matriz[j][0] != matriz[j][k] and matriz[j][k] == "." and matriz[j][k] != '*': #Se muestra la bomba para el modo practica
                # if matriz[0][k] != matriz[j][k] and matriz[j][0] != matriz[j][k] and matriz[j][k] == "." and matriz[j][k] == '*':  # Aquí se agregan "-" al tablero
                print(" ", "-", end=" ")
            else:
                print(" ", matriz[j][k], end=" ")  # Se muestran numero guías para el eje y
        print()

#Obtner las minas cercanas de cada posicion ingresada
def obtener_minas_cercanas(fila, columna):
    cuenta = 0
    if fila <= 1:
        fila_inicio = 1
    else:
        fila_inicio = fila -1
    if fila + 1 >= len(matriz):
        fila_fin = len(matriz) - 1
    else:
        fila_fin = fila+1

    if columna <= 1:
        columna_inicio = 1
    else:
        columna_inicio = columna -1

    if columna + 1 >= len(matriz):
        columna_fin = (len(matriz) -1)
    else:
        columna_fin = columna+1

    for f in range(fila_inicio, fila_fin+1):
        for c in range(columna_inicio, columna_fin+1):
            # Si es la central, la omitimos
            if f == fila and c == columna:
                continue
            elif matriz[f][c] == "*":
                cuenta += 1
    return str(cuenta)

#Parte logica del juego: insecion de coordenadas, busqueda de minas cerca, suma de puntos y cambio de simbolo en el tablero.
def jugarBuscaminas():
    ubicacionCursor = []
    coordenadas = input('Ingresar posición por jugar: ')
    ubicacionesJugadas.append(coordenadas.upper())
    global perdio
    for fila in range(len(matriz)):
        for columna in range(len(matriz)):
            if coordenadas[0].upper() == matriz[fila][columna]:
                ubicacionCursor.append(fila)
                ubicacionCursor.append(columna)
            elif int(coordenadas[1]) == matriz[fila][columna]:
                ubicacionCursor[0] = fila
    f = ubicacionCursor[0]
    c = ubicacionCursor[1]
    global puntos
    aumenta = False
    for i in range(1,len(matriz)-1):
        for j in range(1, len(matriz)-1):
            if matriz[f][c] == "*" and matriz[i][j] == matriz[c][j]:
                print("Has Perdido")
                perdio = True
                break
            elif matriz[i][j] != "*":
                cantMinas = obtener_minas_cercanas(f, c)
                matriz[f][c] = cantMinas
                aumenta = True
    if aumenta == True:
        puntos += 1
    return perdio

#Reiniciar valores de variables globales
def limpiarTablero():
    global matriz
    matriz = []
    global puntos
    puntos = 0
    global nivel
    nivel = 1
    global perdio
    perdio = False
    global puntosGeneral
    puntosGeneral = 0
    global  ubicacionesJugadas
    ubicacionesJugadas = []

#e colocan las minas en el tablero
def colocarMinas(bombas):
    #x = rd.randint(1, len(matriz) - 1)
    #y = rd.randint(1, len(matriz) - 1)
    cont = 0
    conta = 0
    while cont < bombas:
        x = rd.randint(1, len(matriz) - 1)
        y = rd.randint(1, len(matriz) - 1)
        if matriz[y][x] == ".":
            matriz[y][x] = "*"
            cont += 1

# Funcionamiento y levantamiento del juego.
def jugabilidad(modoJuego):
    global puntosGeneral
    global puntos
    global  nivel
    global  matriz
    empezo = True
    while empezo == True:
        # validar el nivel de juego
        if nivel == 1:
            if empezo == True: #Se evita la creacion nueva del tablero
                crearTablero(3, 3)
                colocarMinas(1)
            while True:
                tableroBuscaMinas(modoJuego)
                if jugarBuscaminas() == True:
                    input("PRESIONA ENTER PARA REGRESAR AL MENU...")
                    limpiarTablero()
                    print(ubicacionesJugadas)
                    menu()
                if puntos == ((len(matriz) - 1) * (len(matriz) - 1) - 1):
                    print("GANASTE, HAS PASADO AL NIVEL 2")
                    print("Puntos obtenidos en esta partida: ",puntos)
                    puntosGeneral+=puntos
                    nivel += 1
                    matriz = []
                    puntos = 0
                    input("PRESIONA ENTER PARA AVANZAR AL SIGUIENTE NIVEL...")
                    break
        # Juega nivel 2
        elif nivel == 2:
            if empezo == True:
                crearTablero(5, 5)
                colocarMinas(2)
            while True:
                tableroBuscaMinas(modoJuego)
                if jugarBuscaminas() == True:
                    input("PRESIONA ENTER PARA REGRESAR AL MENU...")
                    limpiarTablero()
                    print(ubicacionesJugadas)
                    menu()
                if puntos == ((len(matriz) - 1) * (len(matriz) - 1) - 2):
                    print("GANASTE, HAS PASADO AL NIVEL 3")
                    print("Puntos obtenidos en esta partida: ", puntos)
                    puntosGeneral += puntos
                    nivel += 1
                    matriz = []
                    puntos = 0
                    input("PRESIONA ENTER PARA AVANZAR AL SIGUIENTE NIVEL...")
                    break
        # Juega nivel 3
        elif nivel == 3:
            if empezo == True:
                crearTablero(7, 7) #Se agrega un numero extra porque no se cuetan las letras y los numeros de coordernadas.
                colocarMinas(4)
            while True:
                tableroBuscaMinas(modoJuego)
                if jugarBuscaminas() == True:
                    input("PRESIONA ENTER PARA REGRESAR AL MENU...")
                    limpiarTablero()
                    print(ubicacionesJugadas)
                    menu()

                if puntos == ((len(matriz) - 1) * (len(matriz) - 1) - 4): # Se multiplican las dimesiones de la matriz y se le resta las cantidad de bombas para tener la cantidad puntos disponibles
                    print("¡HAS GANADO TODOS LOS NIVELES!")
                    print("Puntos obtenidos en esta partida: ", puntos)
                    puntosGeneral += puntos
                    print("-"*50,"\nPuntos Totales: ",puntosGeneral)
                    nivel += 1
                    matriz = []
                    puntos = 0
                    input("PRESIONA ENTER PARA SALIR...")
                    limpiarTablero()
                    print(ubicacionesJugadas)
                    menu()
                    break

jugar = True
pasaNivel = False
def menu():
    try:
        while jugar == True:
            print("Modos de juego disponibles\n1. Modo Test\n2. Modo Prod")
            opc = int(input("Ingrese el modo de juego que desea utilizar: "))
            # validar el modo de juego (Practica u otro.)
            if opc == 1:
               jugabilidad(opc)
            elif opc == 2:
                jugabilidad(opc)
    except ValueError as err:
        print("Error: ",err)
menu()