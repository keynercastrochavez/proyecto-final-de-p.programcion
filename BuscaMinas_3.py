import random as rd

letras = ["A", "B", "C", "D", "E", "F"]
filasM = 5
columnasM = 5
matriz = []
conteo = "1"
gano = False
puntos = 0

def crearTablero(filasMatriz, columnasMatriz):
    for fila in range(filasMatriz):  # Aquí carga la matriz
        matriz.append([])
        for columna in range(columnasMatriz):
            matriz[fila].append('.')


def tableroBuscaMinas():
    x = 0
    y = 0
    xAux = 0  # Se crea una variable auxiliar para para validar que ya el indice 0,0 este en blanco
    for j in range(len(matriz)):
        y = 0
        matriz[j][y] = j
        for k in range(len(matriz)):
            if xAux == 0 and y == 0:  # Se coloca un espcio en blanco en el indice 0,0 de la fila de resferencia para el usuario
                matriz[x][y] = " "
                xAux += 1
                y += 1
            elif xAux != 0 and y != 0 and j < 1:  # Se muestran letras guías eje x
                x = 0
                matriz[x][y] = letras[y - 1]
                y += 1

            if matriz[0][k] != matriz[j][k] and matriz[j][0] != matriz[j][k] and matriz[j][k] == "." and matriz[j][k] != '*':  # Aquí se agregan "-" al tablero
                print(" ", "-", end=" ")
            else:
                print(" ", matriz[j][k], end=" ")  # Se muestran numero guías para el eje y
        print()



def obtener_minas_cercanas(fila, columna):
    cuenta = 0
    if fila <= 1:
        fila_inicio = 1
    else:
        fila_inicio = fila -1
    if fila + 1 >= len(matriz):
        fila_fin = len(matriz) - 1
    else:
        fila_fin = fila+1

    if columna <= 1:
        columna_inicio = 1
    else:
        columna_inicio = columna -1

    if columna + 1 >= len(matriz):
        columna_fin = (len(matriz) -1)
    else:
        columna_fin = columna+1


    for f in range(fila_inicio, fila_fin+1):
        for c in range(columna_inicio, columna_fin+1):
            # Si es la central, la omitimos
            if f == fila and c == columna:
                continue
            elif matriz[f][c] == "*":
                cuenta += 1
    return str(cuenta)


def jugarBuscaminas():
    ubicacionCursor = []
    coordenadas = input('Ingresar posición por jugar: ')

    for fila in range(len(matriz)):
        for columna in range(len(matriz)):
            if coordenadas[0].upper() == matriz[fila][columna]:
                ubicacionCursor.append(fila)
                ubicacionCursor.append(columna)
            elif int(coordenadas[1]) == matriz[fila][columna]:
                ubicacionCursor[0] = fila
    f = ubicacionCursor[0]
    c = ubicacionCursor[1]

    contador = 1
    for i in range(1,len(matriz)-1):
        for j in range(1, len(matriz)-1):
            if matriz[f][c] == "*" and matriz[i][j] == matriz[c][j]:
                print("Has Perdido")
                break
            elif matriz[i][j] != "*":
                cantMinas = obtener_minas_cercanas(f, c)
                matriz[f][c] = cantMinas
        contador+=1




def colocarMinas(nivel):
    #x = rd.randint(1, len(matriz) - 1)
    #y = rd.randint(1, len(matriz) - 1)
    cont = 0
    conta = 0
    if nivel == 1:
        while cont < 1:
            x = rd.randint(1, len(matriz) - 1)
            y = rd.randint(1, len(matriz) - 1)
            if matriz[y][x] == ".":
                matriz[y][x] = "*"
                cont += 1

    elif nivel == 2:
        while conta < 2:
            x = rd.randint(1, len(matriz) - 1)
            y = rd.randint(1, len(matriz) - 1)
            if matriz[y][x] == ".":
                matriz[y][x] = "*"
                conta += 1



crearTablero(5, 5)
colocarMinas(2)


while True:
    tableroBuscaMinas()
    jugarBuscaminas()